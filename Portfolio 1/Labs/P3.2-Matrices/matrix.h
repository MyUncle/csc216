#pragma once
#include <ostream>
#include <iomanip>

template <size_t cols, size_t rows, typename T>
class Matrix{
	private:
		T data[cols][rows];
	public:
		Matrix():data(){};
		Matrix(T input[cols * rows]);
		T &get(size_t x, size_t y){return data[x][y];};
		Matrix<cols, rows, T> operator+(Matrix<cols, rows, T>& o);
		template <size_t cols2>
		Matrix<cols2, rows, T> operator*(Matrix<cols2, cols, T>& o);
		void print(std::ostream& out, size_t width = 10);
		
};

#include "matrix.tcc"
