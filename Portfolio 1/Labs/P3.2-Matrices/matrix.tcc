template <size_t cols, size_t rows, typename T>
Matrix<cols, rows, T>::Matrix(T input[cols * rows]){
	for(size_t i = 0; i < cols; i++){
		for(size_t j = 0; j < rows; j++){
			get(i, j) = input[i*rows + j];
		}
	}
}

template <size_t cols, size_t rows, typename T>
Matrix<cols, rows, T> Matrix<cols, rows, T>::operator+(Matrix<cols, rows, T>& o){
	Matrix<cols, rows, T> rv;
	for(size_t i = 0; i < cols; i++){
		for(size_t j = 0; j < rows; j++){
			rv.get(i, j) = get(i, j) + o.get(i, j);
		}
	}
	return rv;
}

template <size_t cols, size_t rows, typename T>
template <size_t cols2>
Matrix<cols2, rows, T> Matrix<cols, rows, T>::operator*(Matrix<cols2, cols, T>& o){
	Matrix<cols2, rows, T> rv;
	for(size_t i = 0; i < cols2; i++){
		for(size_t j = 0; j < rows; j++){
			T sum = T();
			for(size_t k = 0; k < cols; k++){
				sum = sum + (get(k, j) * o.get(i, k));
			}
			rv.get(i, j) = sum;
		}
	}
	return rv;
}

template <size_t cols, size_t rows, typename T>
void Matrix<cols, rows, T>::print(std::ostream& out, size_t width){
	for(size_t j = 0; j < rows; j++){
		for(size_t i = 0; i < cols; i++){
			out << std::setw(width);
			out << get(i, j);
		}
		out << std::endl;
	}
}
