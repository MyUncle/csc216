#include <iostream>
#include "matrix.h"

using namespace std;

int main(){
	int a1[] = {1,2,3,4,5,6};
	int a2[] = {1,3,5,2,4,6};
	int a3[] = {6,1,5,2,4,3};
	
	Matrix<2, 3, int> a(a1);
	Matrix<2, 3, int> b(a2);
	Matrix<3, 2, int> c(a3);
	
	cout << "A:" << endl;
	a.print(cout, 4);
	cout << "B:" << endl;
	b.print(cout, 4);
	cout << "C:" << endl;
	c.print(cout, 4);
	
	cout << "A + B:" << endl;
	(a + b).print(cout, 4);
	cout << "A * C:" << endl;
	(a * c).print(cout, 4);

	return 0;
}
