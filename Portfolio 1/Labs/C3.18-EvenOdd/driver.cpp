#include <iostream>
#include <vector>

using namespace std;

void printVec(vector<int>& v);
void arrange(vector<int>& v);
void arrange(vector<int>::iterator its, vector<int>::iterator ite);

int main(){
	//Random Integers Generated from Random.org
	int values[] = {12,38,35,41,42,48,16,32,5 ,28,29,34,4 ,43,17,40,8 ,15,34,48,30,33,29,1 ,25,14,3 ,7 ,5 ,4 ,33,47,16,10,50,37,7 ,41,26,16,17,8 ,28,41,19,22,19,19,15,17};
	vector<int> v(values, values + sizeof(values)/sizeof(int));

	cout << "Before rearrangement" << endl;
	printVec(v);
	cout << endl;
	arrange(v);
	cout << "Post rearrangement" << endl;
	printVec(v);
}

void printVec(vector<int>& v){
	for(size_t i = 0; i < v.size(); i++){
		cout << v[i] << endl;
	}
}

void arrange(vector<int>& v){
	arrange(v.begin(), v.end() - 1);
}

void arrange(vector<int>::iterator its, vector<int>::iterator ite){ //Arranges a vector to have all even numbers before odd ones
	if(its < ite){
		if(*its % 2){ //If current position to check is odd
			while(*ite % 2 && its < ite){
				ite--;
			}
			if(its < ite){
				int tmp = *its;
				*its = *ite;
				*ite = tmp;
				arrange(its, ite);
			}
		}else{
			while(*its % 2 == 0 && its != ite){
				its++;
			}
			arrange(its, ite);
		}
	}
}
