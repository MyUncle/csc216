#pragma once
#include <string>
#include <ostream>

class gamedata{
	private:
		std::string symbol;
		int score;
	public:
		gamedata(std::string n = "", int s = 0):symbol(n), score(s){};
		const std::string getsymbol(){return symbol;};
		const int getscore(){return score;};
		void setsymbol(std::string sym){symbol = sym;};
		void setscore(int s){score = s;};
};

class gamenode{
	private:
		gamedata data;
		gamenode *next, *prev;
	public:
		gamenode():next(NULL), prev(NULL){};
		gamenode(gamedata& dat):data(dat), next(NULL), prev(NULL){};
		gamenode(gamenode& o):data(o.data), next(o.next), prev(o.prev){};
		gamenode& operator=(gamenode& o);
	
	friend class game;
};

class game{
	private:
		gamenode *front, *back;
		size_t length, maxlength;
		gamenode* find(size_t index);
	public:
		game(size_t mlen = 10);
		~game();
		void add(gamedata& d);
		void add(std::string n = "", int s = 0);
		bool remove(); //Remove oldest
		bool remove(size_t index);
		
		void list(std::ostream& out);
};
