#include <cstddef> //NULL
#include "scores.h"

gamenode& gamenode::operator=(gamenode& o){
	data = o.data;
	next = o.next;
	prev = o.prev;
	return *this;
}

//###############################################################

game::game(size_t mlen):front(NULL), back(NULL), length(0), maxlength(mlen){
	front = new gamenode();
	back = front;
}

game::~game(){
	if(length > 0){
		for(gamenode* i = front; i != NULL; i = i -> next){
			delete i;
		}
	}
}

gamenode* game::find(size_t index){
	if(length == 0){
		return NULL;
	}
	
	gamenode* rv;
	if(index < length/2){
		rv = front;
		for(size_t i = 0; i < index; i++){
			rv = rv -> next;
		}
	}else{
		rv = back;
		for(size_t i = length - 1; i < index; i--){
			rv = rv -> prev;
		}
	}
	
	return rv;
}

void game::add(gamedata& d){
	if(length == 0){
		front = new gamenode(d);
		back = front;
		length++;
	}else{
		gamenode* tmp = new gamenode(d);
		front -> prev = tmp;
		tmp -> next = front;
		front = tmp;
		length++;
		if(length > maxlength){
			remove();
		}
	}
}

void game::add(std::string n, int s){
	gamedata d = gamedata(n, s);
	add(d);
}

bool game::remove(size_t index){
	if(length == 0 || index > length - 1){
		return false;
	}
	
	if(index == 0){
		delete front;
		front = front -> next;
		if(front == NULL){
			back = NULL;
		}else{
			front -> prev = NULL;
		}
	}else if(index = length - 1){
		delete back;
		back = back -> prev;
		back -> next = NULL;
	}else{
		gamenode* mark = find(index);
		mark -> next -> prev = mark -> prev;
		mark -> prev -> next = mark -> next;
		delete mark;
	}
	length--;
	return true;
}

bool game::remove(){
	return remove(length - 1);
}

void game::list(std::ostream& out){
	gamenode* cur = front;
	out << "Games:" << std::endl;
	while(cur != NULL){
		out << cur -> data.getsymbol() << ':' << cur -> data.getscore() << std::endl;
		cur = cur -> next;
	}
}
