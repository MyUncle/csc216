#include <iostream>
#include "scores.h"

using namespace std;

int main(){
	/*
	I am using the teams from the 2016 League of Legends World championship.
	The point values assigned are arbitrary, but in order of their standings
	*/
	game list;
	
	list.add("SKT", 141);
	list.add("SSG", 132);
	list.add("ROX", 119);
	list.add("H2K", 99);
	list.add("RNG", 92);
	
	list.list(cout);
	cout << endl;
	
	list.add("EDG", 83);
	list.add("ANL", 64);
	list.add("C9 ", 1);
	
	list.list(cout);
	cout << endl;
	
	list.remove(3);
	list.remove(6);
	
	list.list(cout);
	
	return 0;
}
