#pragma once
#include <ostream>
#include <string>
#include <vector>

class relation;
class attempt;
class summation;

class relation{
	private:
		char key;
		char val;
	public:
		relation(char k, char v);
		const char getKey(){return key;};
		const char getVal(){return val;};
};

class attempt{
	private:
		std::vector<relation> mappings;
		int lhs;
		int rhs;
		bool valid;
	public:
		attempt(std::vector<relation> map, int left, int right):mappings(map), lhs(left), rhs(right), valid(){
			if(lhs == rhs){
				valid = true;
			}else{
				valid = false;
			}
		};
		const void print(std::ostream &out);
		const bool isValid(){return valid;};
		
};

class summation{
	private:
		bool valid;
		std::vector<std::string> addends;
		std::string sum;
		std::vector<attempt> fails;
		std::vector<attempt> successes;
		
		static char validNums[];
		static std::vector<char> allNums;
		std::vector<char> tokens; //Container for all unique key tokens
		
		int parse(std::string input, std::vector<relation> relations);
		void solve(std::vector<char> num, size_t step, std::vector<relation> relationList);
	public:
		summation();
		summation(std::string input);
		void solve();
		const bool isValid(){return valid;};
		void print(std::ostream &out);
};
