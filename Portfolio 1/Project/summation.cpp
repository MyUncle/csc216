#include <cstddef> // NULL
#include <cstdlib> // atoi
#include "summation.h"

relation::relation(char k, char v):key(k), val(v){}

//######################################################################

const void attempt::print(std::ostream &out){
	for(size_t i = 0; i < mappings.size(); i++){
		out << mappings[i].getKey() << " -> " << mappings[i].getVal() << std::endl;
	}
	out << lhs << " = " << rhs << std::endl;
	if(valid){
		out << "SUCCESS!!!" << std::endl;
	}else{
		out << "Failure..." << std::endl;
	}
}

//######################################################################

char summation::validNums[] = {'0','1','2','3','4','5','6','7','8','9'};
std::vector<char> summation::allNums = std::vector<char> (validNums, validNums + sizeof(validNums)/sizeof(char));

summation::summation():valid(false), addends(), sum(), fails(), successes(), tokens(){}

summation::summation(std::string input):valid(true), addends(), sum(), fails(), successes(), tokens(){
	size_t plusPos;
	size_t equalPos;
	plusPos = input.find('+');
	equalPos = input.find('=');
	if(plusPos == std::string::npos || equalPos == std::string::npos || equalPos <= plusPos){
		valid = false;
	}else{
		addends.push_back(input.substr(0, plusPos));
		addends.push_back(input.substr(plusPos + 1, equalPos - plusPos));
		sum = input.substr(equalPos + 1);

		for(size_t i = 0; i < input.length(); i++){
			if(input[i] != '+' && input[i] != '=' && input[i] != ' '){
				if(input.find_first_of(input[i]) == i){
					tokens.push_back(input[i]);
				}
			}
		}

		if(tokens.size() > 10){
			valid = false;
		}
	}
}

int summation::parse(std::string input, std::vector<relation> relations){
	for(size_t i = 0; i < relations.size(); i++){
		char key = relations[i].getKey();
		while(input.find(key) != std::string::npos){
			input.replace(input.find(key), 1, 1, relations[i].getVal());
		}
	}
	return atoi(input.c_str());
}

void summation::solve(){
	fails.clear();
	successes.clear();
	solve(allNums, 0, std::vector<relation>());
}

void summation::solve(std::vector<char> num, size_t step, std::vector<relation> relationList){
	if(step < tokens.size() - 1){
		for(size_t i = 0; i < num.size(); i++){
			std::vector<char> num_cpy = num;
			num_cpy.erase(num_cpy.begin() + i);
			std::vector<relation> relations = relationList;
			relations.push_back(relation(tokens[step], num[i]));

			solve(num_cpy, step + 1, relations); // Recursive Invocation & Reduction
		}

	}else{ //Base Case
		for(size_t i = 0; i < num.size(); i++){
			std::vector<relation> relations = relationList;
			relations.push_back(relation(tokens[step], num[i]));

			int lhs = 0;
			int rhs = 0;
			for(size_t j = 0; j < addends.size(); j++){
				lhs += parse(addends[j], relations);
			}
			rhs = parse(sum, relations);
			attempt a = attempt(relations, lhs, rhs);
			if(lhs == rhs){
				successes.push_back(a);
			}else{
				fails.push_back(a);
			}
		}
	}
}

void summation::print(std::ostream &out){
	for(size_t i = 0; i < successes.size(); i++){
		successes[i].print(out);
		out << std::endl;
	}
	out << std::endl << "Successful Attempts: " << successes.size() << std::endl;
	out << "Failed Attempts: " << fails.size() << std::endl;
}
