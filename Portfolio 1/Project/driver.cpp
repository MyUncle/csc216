#include <string>
#include <iostream>
#include "summation.h"
#include "io.h"

int main(){
	using namespace std;
	
	string puzzle;
	summation s;
	
	do{
		cout << "Input a Summation Puzzle: ";
		cin >> puzzle;
		s = summation(puzzle);
		if(!s.isValid()){
			cout << "Not a valid Summation Puzzle. Please Try again" << endl;
		}
	}while(!s.isValid());
	s.solve();
	s.print(cout);
	return 0;
}
