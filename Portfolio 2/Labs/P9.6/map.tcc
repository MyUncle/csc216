
template<typename ktype, typename vtype>
void skipNode<ktype, vtype>::shiftLeft(){
	if(down){
		down->shiftLeft();
	}else{
		(*level)--;
		if(next){
			next->shiftLeft();			
		}
	}
}

template<typename ktype, typename vtype>
void skipNode<ktype, vtype>::shiftRight(){
	if(down){
		down->shiftRight();
	}else{
		(*level)++;
		if(next){
			next->shiftRight();
		}
	}
}

//-----------------------------------------------------------------

template<typename ktype, typename vtype>
map<ktype, vtype>::map():head(NULL), count(0), printMode(false){
	head = new skipNode<ktype, vtype>;
	head->level = new size_t(0);
}

template<typename ktype, typename vtype>
void map<ktype, vtype>::print(std::ostream& out, skipNode<ktype, vtype>* cursor = NULL){
	if(printMode){
		skipNode<ktype, vtype>* root = head;
		skipNode<ktype, vtype>* cur = root;
		while(root){
			cur = root;
			while(cur){
				size_t lev = *cur->level;
				if(cur == cursor){
					out << "{>(";
				}else{
					out << "  (";
				}
				
				if(*cur->level == 0){
					out << "HEAD";
				}else{
					out << std::setw(3) << *cur->key << "|" << std::setw(3) << *cur->data;
				}
				
				if(cur == cursor){
					out << ")<}-";
				}else{
					out << ")  -";
				}
				
				cur = cur->next;
				if(cur){
					lev = *cur->level - lev - 1;
					if(lev > 0){
						out << std::setfill('-') << std::setw(14 * lev) << '-' << std::setfill(' ');
					}
				}
			}
			out << std::endl;
			root = root->down;
		}
		out << std::endl;
	}
}

template<typename ktype, typename vtype>
bool map<ktype, vtype>::empty(){
	if(count > 0){
		return false;
	}
	return true;
}

template<typename ktype, typename vtype>
size_t map<ktype, vtype>::size(){
	return count;
}

template<typename ktype, typename vtype>
vtype& map<ktype, vtype>::find(ktype key){
	skipNode<ktype, vtype>* cur = head;
	skipNode<ktype, vtype>* mem = cur;
	
	print(std::cout, cur);
	
	if(empty()){
		throw std::logic_error("map is empty");
	}
	do{
		mem = cur;
		if(cur->next){
			cur = cur->next;
			print(std::cout, cur);
			if(*cur->key == key){
				return *cur->data;
			}else if(key < *cur->key){
				cur = mem;
				if(cur->down){
					cur = cur->down;
					print(std::cout, cur);
				}else{
					throw std::logic_error("key not found");
				}
			}
		}else if(cur->down){
			cur = cur->down;
			print(std::cout, cur);
		}else{
			throw std::logic_error("key not found");
		}
	}while(true);
}

template<typename ktype, typename vtype>
vtype& map<ktype, vtype>::add(ktype key, vtype value){
	skipNode<ktype, vtype>* cur = head;
	skipNode<ktype, vtype>* mem = cur;
	skipNode<ktype, vtype>* top = NULL;
	
	print(std::cout, cur);
	
	if(empty()){ //Special case for empty list
		ktype* pkey = new ktype(key);
		vtype* pval = new vtype(value);
		size_t* lvl = new size_t(1);
		top = new skipNode<ktype, vtype>(pkey, pval, lvl);
		head->next = top;
		top->prev = head;
		print(std::cout, top);
		count++;
	}
	while(!top){ //Find an insertion point and add the base of tower
		mem = cur;
		if(cur->next){
			cur = cur->next;
			print(std::cout, cur);
			if(*cur->key == key){//Key existed already, so simply replace value data and return
				*cur->data = value;
				return *cur->data;
			}else if(key < *cur->key){
				if(mem->down){
					cur = mem->down;
					print(std::cout, cur);
				}else{
					ktype* pkey = new ktype(key);
					vtype* pval = new vtype(value);
					size_t* lvl = new size_t(*mem->level + 1);
					top = new skipNode<ktype, vtype>(pkey, pval, lvl);
					print(std::cout, top);
					top->prev = mem;
					top->next = cur;
					mem->next = top;
					cur->prev = top;
					cur->shiftRight();
					count++;
				}
			}
		}else if(cur->down){
			cur = cur->down;
			print(std::cout, cur);
		}else{
			ktype* pkey = new ktype(key);
			vtype* pval = new vtype(value);
			size_t* lvl = new size_t(*cur->level + 1);
			top = new skipNode<ktype, vtype>(pkey, pval, lvl);
			print(std::cout, top);
			top->prev = cur;
			cur->next = top;
			count++;
		}
	}
	while(true){// Loop until tower is complete
		if(std::rand() % 2){//Flip a coin to determine whether to add a layer
			mem = top;
			top = new skipNode<ktype, vtype>(mem->key, mem->data, mem->level);
			print(std::cout, top);
			mem->up = top;
			top->down = mem;
			cur = mem->prev;
			while(!cur->up){
				if(cur->prev){
					cur = cur->prev;
				}else{
					mem = new skipNode<ktype, vtype>;
					mem->level = head->level;
					head = mem;
					head->down = cur;
					cur->up = head;
				}
			}
			mem = cur->up;
			if(mem->next){
				cur = mem->next;
				top->next = cur;
				cur->prev = top;
			}
			mem->next = top;
			top->prev = mem;
		}else{
			return *top->data;
		}
	}
}	


template<typename ktype, typename vtype>
void map<ktype, vtype>::remove(ktype key){
	skipNode<ktype, vtype>* cur = head;
	skipNode<ktype, vtype>* mem = cur;
	
	print(std::cout, cur);
	
	bool complete = false;
	if(empty()){
		throw std::logic_error("map is empty");
	}
	do{
		mem = cur;
		if(cur->next){
			cur = cur->next;
			print(std::cout, cur);
			if(*cur->key == key){
				do{
					mem = cur;
					cur = mem->down;
					if(mem->prev){
						mem->prev->next = mem->next;
					}
					if(mem->next){
						mem->next->prev = mem->prev;
					}
					if(!cur){
						delete mem->data;
						delete mem->key;
						delete mem->level;
						if(mem->next){
							mem->next->shiftLeft();
						}
					}
					delete mem;
					print(std::cout, cur);
				}while(cur);
				complete = true;
				count--;
			}else if(key < *cur->key){
				cur = mem;
				if(cur->down){
					cur = cur->down;
					print(std::cout, cur);
				}else{
					throw std::logic_error("key not found");
				}
			}
		}else if(cur->down){
			cur = cur->down;
			print(std::cout, cur);
		}else{
			throw std::logic_error("key not found");
		}
	}while(!complete);
	//Check to see if head can shrink now or not
	while(!head->next && head->down){
		mem = head;
		head = mem->down;
		delete mem;
		print(std::cout, NULL);
	}
}
