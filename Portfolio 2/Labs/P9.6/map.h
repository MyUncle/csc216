#pragma 
#include <iostream>
#include <iomanip>
#include <cstddef>
#include <stdexcept>
#include <cstdlib>
#include <ctime>

template<typename ktype, typename vtype>
class map;

template<typename ktype, typename vtype>
class skipNode{
	friend class map<ktype, vtype>;
	vtype* data;
	ktype* key;
	size_t* level;
	skipNode* next;
	skipNode* prev;
	skipNode* down;
	skipNode* up;
	
	skipNode():data(NULL), key(NULL), level(NULL), next(NULL), prev(NULL), down(NULL), up(NULL){};
	skipNode(ktype* k, vtype* v, size_t* l):data(v), key(k), level(l), next(NULL), prev(NULL), down(NULL), up(NULL){};
	void shiftLeft();
	void shiftRight();
};

template<typename ktype, typename vtype>
class map{
	private:
		skipNode<ktype, vtype>* head;
		size_t count;
		bool printMode;
	public:
		map();
		void print(std::ostream& out, skipNode<ktype, vtype>* cursor);
		bool empty();
		size_t size();
		vtype& find(ktype key);
		vtype& add(ktype key, vtype value);
		void remove(ktype key);
		void printmode(bool b){printMode = b;};
};

#include "map.tcc"
