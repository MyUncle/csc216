#include "map.h"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>

using namespace std;

bool process(map<int, size_t>& m);

int main(){
	map<int, size_t> m;
	srand(time(NULL));
	for(size_t i = 0; i < 20; i++){
		int temp = rand()%20 + 1;
		m.add(temp, i);
	}
	
	m.printmode(true);
	
	while(process(m)){}
	
	return 0;
}

bool process(map<int, size_t>& m){
	string command;
	string first;
	
	
	cout << ">>" << flush;
	getline(cin, command);
	
	if(command.empty()){
		return true;
	}
	
	first = command.substr(0,command.find_first_of(" \n"));
	if(first == "d" || first == "del" || first == "delete"){
		try{
			char* ptr;
			const char* c = command.substr(command.find_first_of(" \n")).c_str();
			int p1 = strtol(c, &ptr, 10);
			if(c == ptr){
				throw logic_error("Invalid Command Parameters");
			}
			m.remove(p1);
			cout << "removed data/key pair found at key: " << p1 << endl; 
		}catch(const out_of_range& e){
			cerr << "Out of Range error: " << e.what() << "\n";
		}catch(const invalid_argument& e){
			cerr << "Invalid Argument error: " << e.what() << "\n";
		}catch(const logic_error& e){
			cerr << "Logic error: " << e.what() << "\n";
		}
		
	}else if(first == "a" || first == "add"){
		try{
			char* ptr;
			char* ptr2;
			const char* c = command.substr(command.find_first_of(" \n")).c_str();
			int p1 = strtol(c, &ptr, 10);
			if(c == ptr){
				throw logic_error("Invalid Command Parameters");
			}
			size_t p2 = strtol(ptr, &ptr2, 10);
			if(ptr == ptr2){
				throw logic_error("Invalid Command Parameters");
			}
			m.add(p1, p2);
			cout << "added " << p2 << " to map with key: " << p1 << endl;
		}catch(const out_of_range& e){
			cerr << "Out of Range error: " << e.what() << "\n";
		}catch(const invalid_argument& e){
			cerr << "Invalid Argument error: " << e.what() << "\n";
		}catch(const logic_error& e){
			cerr << "Logic error: " << e.what() << "\n";
		}
	}else if(first == "f" || first == "find"){
		try{
			char* ptr;
			const char* c1 = command.substr(command.find_first_of(" \n")).c_str();
			int p1 = strtol(c1, &ptr, 10);
			if(c1 == ptr){
				throw logic_error("Invalid Command Parameters");
			}
			size_t rv = m.find(p1);
			cout << rv << endl;
		}catch(const out_of_range& e){
			cerr << "Out of Range error: " << e.what() << "\n";
		}catch(const invalid_argument& e){
			cerr << "Invalid Argument error: " << e.what() << "\n";
		}catch(const logic_error& e){
			cerr << "Logic error: " << e.what() << "\n";
		}
	}else if(first == "q" || first == "quit" || first == "e" || first == "exit"){
		return false;

	}else if(first == "p" || first == "print"){
		m.print(cout);
	}else if(first == "h" || first == "help" || first == "?"){
		cout << "Help Page:" << '\n'
			<< "Add -> {a/add} [key] [value] -- adds a key/value pair to map" << '\n'
			<< "Delete -> {d/del/delete} [key] -- deletes a key/value pair found at given key" << '\n'
			<< "Find -> {f/find} {key} -- finds the value associated with given key" << '\n'
			<< "Help -> {h/help/?} -- Provides this help menu" << '\n'
			<< "Quit -> {q/quit/e/exit} -- Quits the program" << endl;
	}else{
		cout << '[' << first << ']' << "is not a recognized command" << endl;
	}
	return true;
}
