#pragma once
#include <vector>

class Maze{
	private:
		std::vector<std::vector<char> > data;
	public:
		Maze(size_t w, size_t h, char fill = '#'):data(){
			std::vector<char> tmp (w, fill);
			data = std::vector<std::vector<char> > (h, tmp);
		};
		std::vector<std::vector<char> >::iterator begin(){
			return data.begin();
		}
		std::vector<std::vector<char> >::iterator end(){
			return data.end();
		}
		size_t size(){return data.size();};
		
		std::vector<char>& operator[](size_t i){return data[i];};
};
