#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <stack>
#include <iostream>
#include "generator.h"

bool Point::operator<(const Point& rhs) const{
	return index() < rhs.index();
}

size_t Point::index() const{
	size_t n = x + y;
	return n*(n+1)/2 + y;
}

std::istream &operator>>(std::istream &in, Point &p){
	double x;
	double y;
	bool fail = false;
	if(in.get() != '('){
		fail = true;
		in.setstate(std::ios::failbit);
	}
	if(!fail){
		in >> x;
		if(in.fail()){
			fail = true;
		}
	}
	if(!fail){
		if(in.get() != ','){
			fail = true;
			in.setstate(std::ios::failbit);
		}
	}
	if(!fail){
		in >> y;
		if(in.fail()){
			fail = true;
		}
	}
	if(!fail){
		if(in.get() != ')'){
			fail = true;
			in.setstate(std::ios::failbit);
		}
	}
	if(!fail){ //Extraction successful
		p.x = x;
		p.y = y;
	}
	return in;
}

Point pIn(std::string prompt){
	Point rv;
	do{
		if(std::cin.fail()){
			std::cerr << std::endl << "Error: Incorrect Data Type" << std::endl;
			std::cout << "Re-enter data\n";
		}
		std::cin.clear();
		std::cin.ignore(256,'\n');
		std::cout << prompt;
		std::cin >> rv;
	}while(std::cin.fail());
	return rv;
}

//#################################################################

Edge::Edge(Node* n1, Node* n2, int w):a(n1), b(n2), weight(w){
	a -> potentials.push_back(this);
	b -> potentials.push_back(this);
}

bool Edge::link(Node* n){
	if(n == a){
		a -> children.push_back(b);
		b -> parent = a;
		return true;
	}else if(n == b){
		b -> children.push_back(a);
		a -> parent = b;
		return true;
	}
	return false;
}

Edge::~Edge(){
	for(size_t i = 0; i < a -> potentials.size(); i++){
		if(a -> potentials[i] == this){
			a -> potentials.erase(a -> potentials.begin() + i);
		}
	}
	for(size_t i = 0; i < b -> potentials.size(); i++){
		if(b -> potentials[i] == this){
			b -> potentials.erase(b -> potentials.begin() + i);
		}
	}
}

bool Edge::operator<(const Edge& rhs) const{
	return weight < rhs.weight;
}

//#################################################################

//#################################################################

NodeMap::NodeMap(size_t x, size_t y, Point r):root(), verts(), walls(), visited(), maze(Maze(x * 2 - 1, y * 2 - 1)){
	srand(time(NULL));
	for(size_t i = 0; i < x + y - 1; i++){
		for(size_t j = 0; j < std::min(std::min(x, y), std::min(i + 1, x + y - i - 1)); j++){
			Point p = Point((std::min(i, y - 1) - j) * 2, (j + std::max(0, ((int)i + 1 - (int)y))) * 2);
			verts[p] = new Node(p);
			if(i > 0){
				if(p.y > 0){
					Edge* e = new Edge(verts[p], verts[Point(p.x, p.y - 2)], rand() % (x * y));
				}
				if(p.x > 0){
					Edge* e = new Edge(verts[p], verts[Point(p.x - 2, p.y)], rand() % (x * y));
				}
			}
		}
	}
	root = verts[Point(r.y * 2, r.x * 2)];
}

void NodeMap::generate(){
	Node* cur = root;
	visited.push_back(cur);
	do{
		for(size_t i = 0; i < cur -> potentials.size(); i++){
			walls.push_back(cur -> potentials[i]);
		}
		bool cont = true;
		while(cont && !walls.empty()){
			size_t lowindex = 0;
			int lowvalue = walls[0] -> weight;
			for(size_t i = 1; i < walls.size(); i++){
				if(walls[i] -> weight < lowvalue){
					lowvalue = walls[i] -> weight;
					lowindex = i;
				}
			}
			
			for(size_t i = 0; i < visited.size() && cont; i++){
				size_t count = 0;
				bool a = false;
				if(visited[i] == walls[lowindex] -> a){
					count++;
					cur = walls[lowindex] -> b;
					a = true;
				}
				if(visited[i] == walls[lowindex] -> b){
					count++;
					cur = walls[lowindex] -> a;
				}
				if(count == 1){
					cont = false;
					bool curvisited = false;
					for(size_t j = 0; j < visited.size() && !curvisited; j++){
						if(cur == visited[j]){
							curvisited = true;
						}
					}
					if(!curvisited){
						if(a){
							walls[lowindex] -> link(walls[lowindex] -> a);
						}else{
							walls[lowindex] -> link(walls[lowindex] -> b);
						}
						visited.push_back(cur);
					}
					delete walls[lowindex];
					walls.erase(walls.begin() + lowindex);
				}
			}
		}
	}while(!walls.empty());
	
	std::stack<Node*> stk;
	stk.push(root);
	do{
		Node* cur = stk.top();
		stk.pop();
		
		maze[cur -> pos.x][cur -> pos.y] = ' ';
		for(size_t i = 0; i < cur -> children.size(); i++){
			maze[(cur -> pos.x + cur -> children[i] -> pos.x) / 2][(cur -> pos.y + cur -> children[i] -> pos.y) / 2] = ' ';
			stk.push(cur -> children[i]);
		}
	}while(!stk.empty());
}

void NodeMap::solve(Point p){
	Node* cur = verts[Point((p.y - 1) * 2, (p.x - 1) * 2)];
	maze[cur -> pos.x][cur -> pos.y] = '.';
	while(cur != root){
		maze[(cur -> pos.x + cur -> parent -> pos.x) / 2][(cur -> pos.y + cur -> parent -> pos.y) / 2] = '.';
		cur = cur -> parent;
		maze[cur -> pos.x][cur -> pos.y] = '.';
	}
}

void NodeMap::print(std::ostream& out){
	for(size_t i = 0; i < maze.size() + 2; i++){
		out << '#';
		for(size_t j = 0; j < maze[0].size(); j++){
			if(i == 0 || i == maze.size() + 1){
				out << '#';
			}else{
				out << maze[i - 1][j];
			}
		}
		out << '#';
		out << std:: endl;
	}
	out << std::endl;
}

void NodeMap::printn(std::ostream& out){
	for(size_t i = 0; i < visited.size(); i++){
		Point p = visited[i] -> pos;
		out << '(' << p.x / 2 << ',' << p.y / 2 << ')' << std::endl;
	}
}
