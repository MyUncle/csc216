#pragma once
#include <ostream>
#include <istream>
#include <vector>
#include <string>
#include <map>
#include "maze.h"

struct Point;
class Edge;
class Node;
class NodeMap;

struct Point{
	public:
		Point(size_t inx = 0, size_t iny = 0):x(inx), y(iny){};
		size_t x, y;
		bool operator<(const Point& rhs) const;
		size_t index() const;
};

std::istream &operator>>(std::istream &in, Point &p);
Point pIn(std::string prompt);

class Edge{
	private:
		int weight;
		Node *a, *b;
	public:
		Edge(Node* n1, Node* n2, int w = 1);
		bool link(Node* n);
		~Edge();
		bool operator<(const Edge& rhs) const;
	friend class Node;
	friend class NodeMap;
};

class Node{
	private:
		Point pos;
		Node* parent;
		std::vector<Node*> children;
		std::vector<Edge*> potentials;
	public:
		Node(Point p = Point()):pos(p), potentials(), parent(this), children(){};
		Node(size_t x, size_t y):pos(Point(x,y)), potentials(), parent(this), children(){};
		void addPot(Edge* e){potentials.push_back(e);};
	friend class Edge;
	friend class NodeMap;
};

class NodeMap{
	private:
		std::vector<Edge*> walls;
		std::vector<Node*> visited;
		std::map<Point, Node*> verts;
		Node* root;
		Maze maze;
	public:
		NodeMap(size_t x = 25, size_t y = 25, Point r = Point());
		void generate();
		void solve(Point p);
		void print(std::ostream& out);
		void printn(std::ostream& out);
		
};
