#include <iostream>
#include "generator.h"
#include "io.h"

using namespace std;

int main(){
	int w = iIn("Width of maze (min 2): ", "Minimum size is 2", 2);
	int h = iIn("Height of maze (min 2): ", "Minimum size is 2", 2);
	Point s = pIn("Starting Point: ");
	Point e = pIn("Ending Point: ");
	NodeMap N(w, h, s);
	N.generate();
	N.print(cout);
	N.solve(e);
	N.print(cout);
	
	return 0;
}
