#pragma once
#include <string>

//Input
int iIn(std::string prompt);
int iIn(std::string prompt, std::string err, int  min);
int iIn(std::string prompt, std::string err, int  min, int  max);
double fIn(std::string prompt);
double fIn(std::string prompt, std::string err, double min);
double fIn(std::string prompt, std::string err, double min, double max);
bool bIn(std::string prompt);
