#include <iostream>
#include <string>
#include <climits>
#include <sstream>

//Input
int iIn(std::string prompt){
	int rv;
	do{
		if(std::cin.fail()){
			std::cin.clear();
			std::cin.ignore(256,'\n');
			std::cerr << std::endl << "Error: Incorrect Data Type" << std::endl;
			std::cout << "Re-enter data\n";
		}
		std::cout << prompt;
		std::cin >> rv;
	}while(std::cin.fail());
	return rv;
}

int iIn(std::string prompt, std::string err, int min){
	while(true){
		int test = iIn(prompt);
		if(test >= min)
			return test;
		std::cerr << err << std::endl;
	}
}

int iIn(std::string prompt, std::string err, int min, int max){//Setting min >= max ignores min
	while(true){
		int test = iIn(prompt);
		if((test >= min || min >= max) && test <= max)
			return test;
		std::cerr << err << std::endl;
	}
}

double fIn(std::string prompt){
	double rv;
	do{
		if(std::cin.fail()){
			std::cin.clear();
			std::cin.ignore(256,'\n');
			std::cerr << std::endl << "Error: Incorrect Data Type" << std::endl;
			std::cout << "Re-enter data\n";
		}
		std::cout << prompt;
		std::cin >> rv;
	}while(std::cin.fail());
	return rv;
}

double fIn(std::string prompt, std::string err, double min){
	while(true){
		double test = fIn(prompt);
		if(test >= min)
			return test;
		std::cerr << err << std::endl;
	}
}

double fIn(std::string prompt, std::string err, double min, double max){//Setting min >= max ignores min
	while(true){
		double test = fIn(prompt);
		if((test >= min || min >= max) && test <= max)
			return test;
		std::cerr << err << std::endl;
	}
}

bool bIn(std::string prompt){
	bool rv;
	char in;
	bool done = false;
	do{
		if(std::cin.fail()){
			std::cin.clear();
			std::cin.ignore(256,'\n');
			std::cerr << std::endl << "Error: Incorrect Data Type" << std::endl;
			std::cout << "Re-enter data\n";
		}
		std::cout << prompt;
		std::cin >> in;
		if(in == 'y' || in == 'Y'){
			rv = true;
			done = true;
		}else if(in == 'n' || in == 'N'){
			rv = false;
			done = true;
		}else{
			std::cout << "y/n/Y/N only" << std::endl;
		}
	}while(!done);
	return rv;
}
